package ru.bakhtiyarov.tm.api.repository;

import ru.bakhtiyarov.tm.model.Project;
import ru.bakhtiyarov.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneById(String id);

    Project removeOneByName(String name);

}
