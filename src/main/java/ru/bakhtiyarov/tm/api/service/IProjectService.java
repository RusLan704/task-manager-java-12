package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.model.Project;
import ru.bakhtiyarov.tm.model.Task;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project task);

    void remove(Project task);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneById(String id);

    Project removeOneByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(Integer index, String name, String description);

}
