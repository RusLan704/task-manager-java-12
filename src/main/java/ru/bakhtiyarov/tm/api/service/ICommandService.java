package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
