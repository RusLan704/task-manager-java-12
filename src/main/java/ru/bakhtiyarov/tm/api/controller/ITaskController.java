package ru.bakhtiyarov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

}
